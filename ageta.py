import numpy as np
from grapa.functionals import *
from grapa.constants import *

import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.backend as K

from tensorflow.keras.utils import plot_model
from tensorflow.keras.losses import mse
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Lambda, Input



f=np.load("data_1.npz")

x,a=f["x"],f["a"]

shallvae=False
lr=0.001
epochs=100
batch_size=20
patience=10

data=np.concatenate((x,a),axis=-1)



def sampling(args):
    """Reparameterization trick by sampling from an isotropic unit Gaussian.

    # Arguments
        args (tensor): mean and log of variance of Q(z|X)

    # Returns
        z (tensor): sampled latent vector
    """

    z_mean, z_log_var = args

    ##disable variations
    #return z_mean+z_log_var

    batch = K.shape(z_mean)[0]
    dim = K.int_shape(z_mean)[1]
    # by default, random_normal has mean = 0 and std = 1.0
    epsilon = K.random_normal(shape=(batch, dim))
    return z_mean + K.exp(0.5 * z_log_var) * epsilon



def createmodel():
  i=Input(shape=data.shape[1:])
  gs=int(data.shape[1])
  param=int(data.shape[2])-gs
  g=grap(state(gs=gs,param=param))
  g.X,g.A=gcutparam(gs=gs,param1=param,param2=gs)([i])
  
  m=getm()

  xx=g.X

  g=gnl(g,m)
  
  oparam=g.s.param


  g=compress(g,m,5,10)

  g=gll(g,m,k=1)

  #g,com,i2=graphatbottleneck(g,m)

  #g=gll(g,m,k=1)
  m.decompress="paramlike"

  g,com,i2=decompress(g,m,5)

  g=gnl(g,m)

  g=remparam(g,oparam)


  return handlereturn(i,xx,com,i2,g.X,shallvae)

def getae():
  i1,c,z1,z2,_,i2,o=createmodel()
  if shallvae:
    z=Lambda(sampling,name='z')([z1,z2])
  else:
    z=z1

  encoder=Model(i1,[z1,z2,z],name="encoder")
  decoder=Model(i2,o,name="decoder")

  plot_model(encoder,to_file="encoder.png",show_shapes=True)
  plot_model(decoder,to_file="decoder.png",show_shapes=True)

  o2=decoder(encoder(i1)[2:])

  vae=Model(i1,o2,name="vae")
  

  loss=mse(c,o2)
  loss=K.mean(loss)

  if shallvae:
    kl_loss=-0.5*K.mean(1+z2-K.square(z1)-K.exp(z2))
    loss+=kl_loss
  
  vae.add_loss(loss)
  vae.compile(Adam(lr=lr))
  vae.summary()
  plot_model(vae,to_file="vae.png",show_shapes=True)

  return vae

