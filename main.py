
from ageta import *

vae=getae()

vae.fit(data,
	epochs=epochs,
	batch_size=batch_size,
	validation_split=0.1,
	verbose=1,
	callbacks=[keras.callbacks.CSVLogger("history.csv"),
                   keras.callbacks.EarlyStopping(monitor="val_loss",patience=patience),
                   keras.callbacks.TerminateOnNaN(),
                   keras.callbacks.ModelCheckpoint("model.tf",monitor="val_loss",verbose=1,save_best_only=True,save_weights_only=True)])














