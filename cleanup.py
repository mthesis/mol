import numpy as np
import matplotlib.pyplot as plt


f=np.load("data.npz",allow_pickle=True)



map3={"0.0":0,"H":1,"C":2,"O":3,"N":4}


x=f["x"]
a=f["a"]

toaf=f["f"]
toam=f["m"]
toafn=f["fn"]


l=[len(ax) for ax in x]


s=np.max(l)

#s=30

#print(x[0].shape)
#print(np.zeros((s-len(x[0]),x[)))



xx=np.array([np.concatenate((ax,np.zeros((s-len(ax),np.array(ax).shape[-1]))),axis=0) for ax in x if len(ax)<=s])



print(xx.shape)



for i in range(len(xx)):
    for j in range(len(xx[i])):

        xx[i,j,-1]=map3[str(xx[i,j,-1])]


print(xx.shape)


#print(a[0].shape)
print(s-len(a[0]),len(a[0]))


aq=[]

xq=[]
fq=[]
mq=[]
fnq=[]

print("start")

def transmat(a,x):
    ret=np.zeros((len(x),len(x)))

    for aa in a:
        ret[aa[0]-1,aa[1]-1]=1
        ret[aa[1]-1,aa[0]-1]=1
    
    return ret

def gofloat(q):
    return np.array([[float(qqq) for qqq in qq] for qq in q])

for i,(aa,xxx) in enumerate(zip(a,xx)):
    aa=transmat(aa,xxx)
    aa=np.array(aa)
    #try:
    for wwww in range(1):
        la=len(aa)
        if la>s:continue
        if la<1:continue
        ap=np.zeros((s-la,la))
        #print(aa.shape)
        #print(la)
        #print(ap.shape)
        #print(s)
        #exit()
        aa=np.concatenate((aa,ap),axis=0)
        ap=np.zeros((len(aa),s-la))
        aa=np.concatenate((aa,ap),axis=1)
        aq.append(aa)

        xq.append(gofloat(xx[i]))
        fq.append(toaf[i])
        mq.append(toam[i])
        fnq.append(toafn[i])

        
    #except:
    #    print("failed at",aa.shape)
aq=np.array(aq)


print(aq.shape)

print("fraction",len(aq)/len(a),len(aq))


np.savez_compressed("data_1.npz",a=aq,x=xq,f=fq,m=mq,fn=fnq)







