import numpy as np
import matplotlib.pyplot as plt

import os
from os.path import isfile

from read import read


def statinf(q):
  return {"shape":np.array(q).shape,"mean":np.mean(q),"std":np.std(q),"min":np.min(q),"max":np.max(q)}


fns=["data/"+q for q in os.listdir("data/") if isfile("data/"+q)]


molecules=["N","H","C","O"]
#will only work with one char molecules


lf=len(fns)

ms=[]
cous=[]

hit=0

def readsubformula(q):
  try:
    atom=q[0]
    n=1
    if len(q)>1:
      n=int(q[1:])
    if not atom in molecules:return "fail",0
    return atom,n
  except:
    return "fail",0
def readformula(q):
  # print(q)
  qq=q.split(" ")
  ret={}
  for qqq in qq:
    ac=readsubformula(qqq)
    ret[ac[0]]=ac[1]
  return ret


ux,ua,uf,um,ufn=[],[],[],[],[]


for i,fn in enumerate(fns):
  x,a,f,m=read(fn)


  if m<100:continue
  if m>800:continue
  
  mols=set([xx[-1] for xx in x])
  
  failed=False
  for mol in mols:
    if not mol in molecules:failed=True
  if failed:continue
  
  #1845
  if not "H" in mols:continue
    
  #1715
  if len(x)>50:continue
  
  lnh=len([xx[-1] for xx in x if xx[-1]!="H"])
  
  #1444
  if lnh>25:continue
  
  #1327
  if lnh<10:continue
  
  #1300
  if len(x)<25:continue
  
  #1205
  
  # print(mols)
  ff=readformula(f)
  fc=np.sum([ff[key] for key in ff.keys()])
  
  if not fc==len(x):continue
  #831
  
  if "fail" in ff.keys():continue
  #828
  
  hit+=1
  ms.append(m)
  cous.append(len(x))
  
  ux.append(x)
  ua.append(a)
  uf.append(f)
  um.append(m)
  ufn.append(fn)
  
  
  print(i/lf)

print("fraction",hit/lf,hit)



# print(statinf(m))
# print(statinf(cous))

np.savez_compressed("data",x=ux,a=ua,f=uf,m=um,fn=ufn)
  







