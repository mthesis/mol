


def read(fn):
  with open(fn,"r") as f:
    t=f.read()
  l=t.split("\n")
  l=l[4:]
  atoms=[]
  conns=[]
  takeform=False
  takemass=False
  for i,ll in enumerate(l):
    ol=[ord(lll) for lll in ll]
    n=0
    for oll in ol:
      if oll==32:
        n+=1
      else:
        break
    if n==4:atoms.append(ll[4:])
    if n==2:conns.append(ll[2:])


    if takeform==True:
      takeform=ll
    if "<Formula>" in ll:
      takeform=True
    if takemass==True:
      takemass=ll
    if "<Mw>" in ll:
      takemass=True


  fatoms=[]
  for atom in atoms:
    ac=[q for q in atom.split(" ") if len(q)>0]
    fatoms.append([float(ac[0]),float(ac[1]),float(ac[2]),ac[3]])
    
  fconns=[]
  for conn in conns:
    ac=[q for q in conn.split(" ") if len(q)>0]
    fconns.append([int(ac[0]),int(ac[1]),int(ac[2])])


  
  
  return fatoms,fconns,takeform,float(takemass)
  

def plotmol(x,a,f):
  import networkx as nx
  import matplotlib.pyplot as plt

  G=nx.Graph()
  
  for i,xx in enumerate(x):
    G.add_node(i+1)
 
  for aa in a:
    G.add_edge(aa[0],aa[1])

  

  nx.draw_networkx(G,with_labels=True)
  plt.title(f)
  plt.show() # display


if __name__=="__main__":
  # x,a,f,m=(read("558604.mol"))
  # x,a,f,m=(read("558605.mol"))
  x,a,f,m=(read("data/1000000.mol"))

  print("mass",m)
  plotmol(x,a,f)









