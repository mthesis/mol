import os
from os.path import isfile
import json

from read import *

fns=["data/"+q for q in os.listdir("data/") if isfile("data/"+q)]


lf=len(fns)


h={}

acc=["N","H","C","O"]

acc.append("S")
acc.append("Cl")
acc.append("F")


#acc=["H","C","O"]

val=0


ll=[]
qll=[]


for i,fn in enumerate(fns):
    a,_,_,_=read(fn)

    lac=len(a)
    qlac=len([aa for aa in a if aa[-1]!="H"])
    #if not lac==qlac:print("yay")


    a=set([aa[-1] for aa in a])

    for aa in a:
        if not aa in h.keys():h[aa]=0
        h[aa]+=1

    print(i/lf)

    failed=False
    for aa in a:
        if not aa in acc:failed=True
    if failed==False:
        val+=1
        ll.append(lac)
        qll.append(qlac)


print(json.dumps(h,indent=2))
    

print(val/lf,val)

#exit()

import matplotlib.pyplot as plt

plt.hist(ll,bins=20,alpha=0.5)
plt.hist(qll,bins=20,alpha=0.5)
plt.show()



