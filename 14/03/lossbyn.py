import numpy as np
import matplotlib.pyplot as plt

import sys
fix=0
if len(sys.argv)>1:
  fix=int(sys.argv[1])



f=np.load("eval.npz")



y,p=f["y"],f["p"]
x=f["x"]
x=x[:,:,:p.shape[-1]]



l=(y-p)**2

l=np.mean(l,axis=1)
l=np.sqrt(l)


ln=[]
qn=[]
xn=[]


for i,ll in enumerate(l):
  ln.append(ll)
  xn.append(x[i])



def gobol(q):
  if q==1 and fix==2:return 1
  if q==2 and fix==3:return 1
  if q==3 and fix==4:return 1
  if q==4 and fix==5:return 1
  if q>2.5 and fix==6:return 1
  if q>0.5 and fix==0:return 1
  if q>1.5 and fix==1:return 1
  return 0

def getn(q):
  return np.sum([gobol(x[-1]) for x in q])

n=[getn(q) for q in xn]


def splitana(n,ln,qn):
  q,l={},{}
  for xn,xln,xqn in zip(n,ln,qn):
    if not xn in q.keys():
      q[xn]=[]
      l[xn]=[]
    q[xn].append(xqn)
    l[xn].append(xln)
  return q,l

sq,sl=splitana(n,ln,qn)



for key in sq.keys():
  print(key,"minima",sq[key][np.argmin(sl[key])],"maxima",sq[key][np.argmax(sl[key])],"len",len(sq[key]))


plt.plot(n,ln,"o",alpha=0.5)


plt.show()








